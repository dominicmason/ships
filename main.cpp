﻿#include <iostream>
#include <vector>
#include <algorithm>
#include <iomanip>
#include <random>
#include <map>
#include <sstream>
#include <fstream>
#include <functional>

std::random_device rd;

class ivec2
{
public:
    int x, y;

    std::vector<ivec2> neighbours () const
    {
        return {
            { x - 1, y - 1 },{ x, y - 1 },{ x + 1, y - 1 },
            { x - 1, y },					{ x + 1, y },
            { x - 1, y + 1 },{ x, y + 1 },{ x + 1, y + 1 }
        };
    }
    std::vector<ivec2> connected () const
    {
        return {
            { x, y - 1 },
            { x, y + 1 },
            { x - 1, y },
            { x + 1, y }
        };
    }
    bool isAdjacent ( const ivec2 pos ) const
    {
        std::vector<ivec2> adj = neighbours();
        return find ( adj.begin(), adj.end(), pos ) != adj.end();
    }
    bool isConnected ( const ivec2 pos ) const
    {
        std::vector<ivec2> con = connected();
        return find ( con.begin(), con.end(), pos ) != con.end();
    }
    bool isWithin (int size) const
    {
        return x < size && y < size;
    }
    ivec2 operator+ ( const ivec2 other ) const
    {
        return ivec2 { x + other.x, y + other.y };
    }
    ivec2 operator- ( const ivec2 other ) const
    {
        return ivec2 { x - other.x, y - other.y };
    }
    bool operator== ( const ivec2 other ) const
    {
        return x == other.x && y == other.y;
    }
    bool operator!= ( const ivec2 other ) const
    {
        return x != other.x || y != other.y;
    }
    ivec2 sign() const
    {
        return { ( ( 0 < x ) - ( x < 0 ) ), ( ( 0 < y ) - ( y < 0 ) ) };
    }
    ivec2 dirFrom ( const ivec2 other ) const
    {
        return ( *this - other ).sign();
    }
    bool isZero() const
    {
        return x == 0 && y == 0;
    }
    bool isStraight() const
    {
        return ! ( x && y );
    }
    friend std::istream &operator>> ( std::istream &in, ivec2 &out )
    {
        in >> out.y;
        in >> out.x;
        return in;
    }
    friend std::ostream &operator<< ( std::ostream &out, const ivec2 &vec )
    {
        out << "(" << vec.y << "," << vec.x << ")";
        return out;
    }
};

enum class tile
{
    NONE, MISS, HIT
};

class Ship
{
public:
    struct section {
        ivec2 pos;
        bool sunk;
    };
    std::vector<section> sections;
    Ship ( std::vector<section> secs )
        :sections ( secs ) {}
    bool sunk() const
    {
        return std::all_of ( sections.begin(), sections.end(), [] ( const section &s ) {
            return s.sunk;
        } );
    }
    bool contacts ( const ivec2 pos ) const
    {
        return std::any_of ( sections.begin(), sections.end(), [pos] ( const section &s ) {
            return s.pos == pos;
        } );
    }
    bool contactsOrAdjacent ( const ivec2 pos ) const
    {
        return std::any_of ( sections.begin(), sections.end(), [pos] ( const section &s ) {
            return s.pos == pos || s.pos.isAdjacent ( pos );
        } );
    }
    bool hit ( const ivec2 pos )
    {
        auto sec = std::find_if ( sections.begin(), sections.end(), [pos] ( const section &s ) {
            return s.pos == pos;
        } );
        if ( sec == sections.end() )
            return false;
        else {
            sec->sunk = true;
            return true;
        }
    }
};

class Target
{
public:
    std::vector<ivec2> hits;
    std::vector<ivec2> misses;
    bool headExplored, tailExplored;
    int boardSize;
    enum class Dir
    {
        UNKNOWN, HORIZ, VERT
    };
    Dir dir;
    Target ( ivec2 start, int size)
        : headExplored ( false ),
          tailExplored ( false ),
          boardSize (size),
          dir ( Dir::UNKNOWN )
    {
        hits.emplace_back ( start );
    }
    void hit ( ivec2 pos )
    {
        hits.emplace_back ( pos );
        if ( dir == Dir::UNKNOWN ) {
            if ( hits[1].dirFrom ( hits[0] ).y == 0 )
                dir = Dir::HORIZ;
            else
                dir = Dir::VERT;
        }
        if ( dir == Dir::HORIZ )
            std::sort ( hits.begin(), hits.end(), [] ( ivec2 lhs, ivec2 rhs ) {
            return lhs.x < rhs.x;
        } );
        else
            std::sort ( hits.begin(), hits.end(), [] ( ivec2 lhs, ivec2 rhs ) {
            return lhs.y < rhs.y;
        } );
    }
    ivec2 getEndPos(bool top)
    {
        ivec2 add;
        if (dir == Dir::HORIZ)
            add = {1,0};
        else
            add = {0,1};
        if (top)
            return hits[hits.size() - 1] + add;
        else
            return hits[0] - add;
    }
    ivec2 getTargetPos()
    {

        if (!headExplored)
            return getEndPos(true);
        else
            return getEndPos(false);
    }
    void miss (ivec2 pos)
    {
        misses.emplace_back(pos);
    }
    bool isExplored() const
    {

        return headExplored && tailExplored;
    }
};

class Board
{
public:
    std::mt19937 gen;
    std::uniform_int_distribution<> dist;
    int size;
    std::vector<Ship> ships;
    std::vector<std::vector<tile>> board;
    std::vector<Target> targets;

    Board ( int boardSize = 10, bool randomShips = true )
        : gen ( rd() ),
          dist ( 0, boardSize - 1 ),
          size ( boardSize ),
          board ( boardSize )
    {
        for ( int i = 0; i < size; i++ ) {
            board[i] = std::vector<tile> ( size );
            for ( int j = 0; j < size; j++ )
                board[i][j] = tile::NONE;
        }
        if ( randomShips )
            placeRandomShips();
    }
    bool checkPosSafe ( const ivec2 pos ) const
    {
        return checkPosOnBoard ( pos ) && !contactsOrAdjacentToShip ( pos );
    }
    bool checkPosOnBoard ( const ivec2 pos ) const
    {
        return pos.x >= 0 && pos.x < size
               && pos.y >= 0 && pos.y < size;
    }
    int createShip ( const ivec2 start, const ivec2 end, bool truncate = false )
    {
        int numSecs = 0;
        std::vector<Ship::section> sections;
        ivec2 dir = end.dirFrom ( start );
        if ( dir.isZero() || !dir.isStraight() ) {
            return 0;
        }
        for ( ivec2 i = start; i != end + dir; i = i + dir ) {
            if ( checkPosSafe ( i ) ) {
                sections.push_back ( { i, false } );
                numSecs++;
                std::cout << i << std::endl;
            } else {
                if ( !truncate )
                    numSecs = 0;
                break;
            }
        }
        if ( numSecs > 1 ) {
            ships.emplace_back ( std::move ( sections ) );
            return numSecs;
        } else
            return 0;
    }
    void placeRandomShips()
    {
        int sections = 0, iters = 0;
        while ( sections < board.size() * 2 && iters < 1000 ) {
            ivec2 start, end;
            int pos1 = dist ( gen );
            if ( dist ( gen ) > board.size() / 2 ) {
                start = { pos1, dist ( gen ) };
                end = { pos1, dist ( gen ) };
            } else {
                start = { dist ( gen ), pos1 };
                end = { dist ( gen ), pos1 };
            }
            sections += createShip ( start, end, true );
            iters++;
        }
        std::cout << sections << " sections" << std::endl;
    }
    bool contactsOrAdjacentToShip ( const ivec2 pos ) const
    {
        return std::any_of ( ships.begin(), ships.end(), [pos] ( const Ship &s ) {
            return s.contactsOrAdjacent ( pos );
        } );
    }
    int shipsLeft() const
    {
        return std::count_if ( ships.begin(), ships.end(), [] ( const Ship &s ) {
            return !s.sunk();
        } );
    }
    void informMiss(ivec2 pos)
    {
        for (Target &t : targets)
            t.miss(pos);
    }
    bool fireAt ( const ivec2 pos )
    {
        auto ship = std::find_if ( ships.begin(), ships.end(), [pos] ( Ship &s ) {
            return s.hit ( pos );
        } );
        if ( ship == ships.end() ) {
            board[pos.x][pos.y] = tile::MISS;
            informMiss(pos);
            return false;
        } else {
            board[pos.x][pos.y] = tile::HIT;
            return true;
        }
    }
    ivec2 getRandomTile ( bool fresh = true )
    {
        ivec2 pos = {dist ( gen ), dist ( gen ) };
        while ( fresh && atPos ( pos ) != tile::NONE ) {
            pos = {dist ( gen ), dist ( gen ) };
        }
        return pos;
    }
    tile atPos ( ivec2 pos ) const
    {
        return board[pos.x][pos.y];
    }
};

class turn
{
public:
    int player;
    int victim;
    ivec2 pos;
    bool hit;
    bool out;
    friend std::istream &operator>> ( std::istream &in, turn &t )
    {
        in >> t.player;
        in >> t.victim;
        in >> t.pos;
        in >> t.hit;
        in >> t.out;
        return in;
    }
    friend std::ostream &operator<< ( std::ostream &outStream, const turn &t )
    {
        outStream << t.player << " " << t.victim << " " << t.pos.y << " " << t.pos.x << " " << t.hit << " " << t.out;
        return outStream;
    }
    std::string explain() const
    {
        std::ostringstream out ( std::ios_base::ate );
        out << "Player " << player;
        out << " fired at player " << victim;
        out << " at " << pos << " which ";
        out << ( hit ? "hit!" : "missed." );
        return out.str();
    }
};

class player
{
public:
    bool human;
    bool out = false;
    bool remote = false;
    int id;
    int turns = 0;
    std::string name;
    std::mt19937 gen;
    Board board;
    struct stat {
        int danger;
        int damage;
        int accuracy;
    };
    std::map<int, stat> stats;
    std::vector<int> playerIDs;
    player ( bool h, int number, int boardSize )
        : human ( h ),
          id ( number ),
          gen ( rd() ),
          board ( boardSize )
    { }
    void initialiseDanger ( const std::vector<player> &players )
    {
        for ( auto &p : players ) {
            if ( p.id != id ) {
                playerIDs.push_back ( p.id );
                stats[p.id] = { 0, 0, 0 };
            }
        }
    }
    void update ( turn t )
    {
        turns++;
        if ( t.player != id && t.victim != id ) {
            if ( t.hit ) {
                stats[t.player].danger += 2;
                stats[t.player].accuracy++;
                stats[t.victim].damage++;
            } else {
                stats[t.player].accuracy--;
                stats[t.player].danger--;
            }
            if ( t.out )
                stats[t.player].danger += 10;
        }
        if ( t.out )
            removeStat ( t.victim );
        for ( auto &s : stats ) {
            s.second.danger--;
        }
    }
    bool fireAt ( const ivec2 pos, int id )
    {
        bool hit = board.fireAt ( pos );
        if ( hit )
            stats[id].danger += 10;
        else
            stats[id].danger += 5;
        return hit;
    }
    void removeStat ( int id )
    {
        stats.erase ( id );
        playerIDs.erase ( std::remove ( playerIDs.begin(), playerIDs.end(), id ), playerIDs.end() );
    }
    int mostDangerousPlayerID() const
    {
        return std::max_element ( stats.begin(), stats.end(), [] ( const std::pair<int, stat> &s1, const std::pair<int, stat> &s2 ) {
            return s1.second.danger < s2.second.danger;
        } )->first;
    }
    int randomPlayerId()
    {
        std::uniform_int_distribution<int> dist ( 0, playerIDs.size() - 1 );
        return playerIDs[dist ( gen )];
    }
    bool dangerEqual() const
    {
        return ( std::adjacent_find ( stats.begin(), stats.end(), [] ( const std::pair<int, stat> &s1, const std::pair<int, stat> &s2 ) {
            return s1.second.danger != s2.second.danger;
        } ) == stats.end() );
    }
    int chooseVictim()
    {
        if ( dangerEqual() )
            return randomPlayerId();
        else
            return mostDangerousPlayerID();
    }
    ivec2 getAim ( player &p ) const
    {
        return p.board.getRandomTile();
    }
};

class Network
{
public:
    void resetNetwork ( std::vector<player> &players )
    {

        updateBoards ( players );
        for ( player &p : players ) {
            if ( p.remote ) {

            }
        }
    }
    void kickOut ( player &p )
    {

    }
    void updateBoards ( std::vector<player> &players )
    {
        for ( player &p : players ) {

        }
    }
    void updateNetwork ( std::vector<player> &players, const turn t, const int turns )
    {

    }
    turn runRemoteTurn ( player &p )
    {

        turn t;

        return t;
    }
};

class textGame
{
public:
    int turns = 0;
    bool networked = false;
    bool running = true;
    Network net;
    std::vector<player> players;
    textGame ()
    {
        int humans, robots;
        std::cout << "Enter the number of human players: ";
        std::cin >> humans;
        std::cout << "Enter the number of robot players: ";
        std::cin >> robots;
        for ( int i = 1; i <= humans; i++ ) {
            int size;
            std::cout << "Enter the board size for human player " << i << ": ";
            std::cin >> size;
            if ( size )
                players.emplace_back ( true, i, size );
        }
        for ( int i = humans + 1; i <= humans + robots; i++ ) {
            int size;
            std::cout << "Enter the board size for robot player " << i << ": ";
            std::cin >> size;
            if ( size )
                players.emplace_back ( false, i, size );
        }
        if ( players.size() < 2 )
            exit ( 0 );
        for ( auto &p : players )
            p.initialiseDanger ( players );
    }
    int playersLeft() const
    {
        return std::count_if ( players.begin(), players.end(), [] ( const player &p ) {
            return !p.out;
        } );
    }
    std::string draw ( const Board& b ) const
    {
        std::ostringstream outBuf ( std::ios_base::ate );
        outBuf << b.size << " x " << b.size << " board:\n";
        outBuf << "  ";
        for ( int i = 0; i < b.board.size(); i++ ) {
            outBuf << std::setw ( 4 ) << i;
        }
        outBuf << "\n";
        for ( int i = 0; i < b.board.size(); i++ ) {
            outBuf << "   ";
            for ( int i = 0; i < b.board.size(); i++ ) {
                outBuf << "+---";
            }
            outBuf << "+\n";
            outBuf << std::setw ( 2 ) << i << " ";
            for ( int j = 0; j < b.board.size(); j++ ) {
                outBuf << "|";
                switch ( b.board[i][j] ) {
                case tile::NONE:
                    outBuf << " ~ ";
                    break;
                case tile::MISS:
                    outBuf << "\\ /";
                    break;
                case tile::HIT:
                    outBuf << " X ";
                    break;
                }
            }
            outBuf << "|\n";
        }
        outBuf << "   ";
        for ( int i = 0; i < b.board.size(); i++ ) {
            outBuf << "+---";
        }
        outBuf << "+\n";
        return outBuf.str();
    }
    int getVictimId ( const player &p )
    {
        if ( playersLeft() > 2 ) {
            while ( true ) {
                int victim;
                std::cout << "Player " << p.id << ", enter the player id to attack: ";
                std::cin >> victim;
                if ( victim > 0 && victim <= players.size() && victim != p.id )
                    return victim;
            }
        } else {
            auto victim = std::find_if ( players.begin(), players.end(), [&p] ( const player &p2 ) {
                return !p2.out && p2.id != p.id;
            } );
            if ( victim != players.end() )
                return victim->id;
            else {
                endGame();
                return p.id;
            }
        }
    }
    void endGame()
    {
        std::cout << "Game Over!" << std::endl;
        running = false;
        auto winner = std::find_if ( players.begin(), players.end(), [] ( const player &p ) {
            return !p.out;
        } );
        if ( winner != players.end() )
            std::cout << "Winner is player " << winner->id << std::endl;
        std::cout << "Won in " << turns << " turns" << std::endl;
    }
    ivec2 aim ( const int victim ) const
    {
        while ( true ) {
            ivec2 pos;
            std::cout << "Enter position to fire at (x y): ";
            std::cin >> pos;
            if ( players[victim].board.checkPosOnBoard ( pos ) )
                return pos;
        }
    }
    void kickOut ( player &p )
    {
        p.out = true;
        std::cout << "Player " << p.id << " is out!" << std::endl;
        if ( p.remote )
            net.kickOut ( p );
        if ( playersLeft() <= 1 )
            endGame();
    }
    turn runTurn ( player &p )
    {
        turn t;
        t.player = p.id;
        std::cout << "\nPlayer " << p.id << "'s turn:" << std::endl;
        for ( player &p : players ) {
            std::cout << "Player " << p.id << " has " << p.board.shipsLeft() << " ships remaining" << std::endl;
        }
        t.victim = getVictimId ( p );
        int victim = t.victim - 1;
        std::cout << "Targeting player " << t.victim << ":" << std::endl;
        std::cout << draw ( players[victim].board );
        t.pos = aim ( victim );
        t.hit = players[victim].fireAt ( t.pos, p.id );
        if ( t.hit )
            std::cout << "Ship Hit!" << std::endl;
        else
            std::cout << "Missed" << std::endl;
        std::cout << draw ( players[victim].board );
        if ( players[victim].board.shipsLeft() == 0 ) {
            t.out = true;
            kickOut ( players[victim] );
        } else
            t.out = false;
        std::cout << std::endl;
        return t;
    }

    turn runRoboTurn ( player &p )
    {
        turn t;
        t.player = p.id;
        std::cout << "\nRobot player " << p.id << "'s Turn:" << std::endl;
        t.victim = p.chooseVictim();
        int victim = t.victim - 1;
        std::cout << "Target acqurired: player " << t.victim << std::endl;
        t.pos = p.getAim ( players[victim] );
        std::cout << "Firing at: " << t.pos << std::endl;
        t.hit = players[victim].fireAt ( t.pos, p.id );
        if ( t.hit )
            std::cout << "Ship Hit!" << std::endl;
        else
            std::cout << "Missed" << std::endl;
        std::cout << draw ( players[victim].board );
        if ( players[victim].board.shipsLeft() == 0 ) {
            t.out = true;
            kickOut ( players[victim] );
        } else
            t.out = false;
        std::cout << std::endl;
        return t;
    }

    void updatePlayers ( const turn t )
    {
        for ( player &p : players ) {
            p.update ( t );
        }
        if ( networked ) {
            net.updateNetwork ( players, t, turns );
        }
    }
    void run()
    {
        if ( networked )
            net.resetNetwork ( players );
        while ( running ) {
            for ( player &p : players ) {
                if ( running && !p.out ) {
                    std::cout << "Turn " << ++turns << ":" << std::endl;
                    if ( p.remote )
                        updatePlayers ( net.runRemoteTurn ( p ) );
                    else if ( p.human )
                        updatePlayers ( runTurn ( p ) );
                    else
                        updatePlayers ( runRoboTurn ( p ) );
                }
            }
        }
    }
};

int main ( int argc, char *argv[] )
{
    std::ios_base::sync_with_stdio ( false );
    textGame game;
    game.run();
    return 0;
}
